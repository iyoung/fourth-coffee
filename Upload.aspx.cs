﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Upload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if there is previous page data, do not make LocationSelector visble to the user, as it won't be needed.
        if (Page.PreviousPage != null)
        {
            LocationSelector.SelectedValue = Page.PreviousPage.Title;
            LocationLabel.Text = "Select a file to upload, it will be saved to " + LocationSelector.SelectedValue + " images.";
            LocationSelector.Visible = false;
        }
        // if previous page is null, prompt the user to make a selection
        else
        {
            LocationLabel.Text = "Select a file and location to save to, then press Send photo.";
        }
    }


    // This method will check to see which page the user was browsing before opening this page. If null, the user will be prompted 
    // for input, which, will select which folder to save jpeg data to.
    protected void SendButton_Click(object sender, EventArgs e)
    {
        string imageFolder;
        string imagePath;
        string fileName;
        string savePath;



        if (LocationSelector.SelectedValue != null)
        {
            imageFolder = "Images";
            LocationLabel.Text = "Your photo will be saved to " + LocationSelector.SelectedValue + " "+ imageFolder + ". Please select a photo and then click send photo.";
        }
        //if a file has been selected
        if (FileUpload1.HasFile)
        {
            try
            {   //if the file is of the specified type
                if (FileUpload1.PostedFile.ContentType == "image/jpeg")
                {   //if the file size is less than 1MB
                    if (FileUpload1.PostedFile.ContentLength < 1024000)
                    {   //builds a filepath to save the photo, saves the file to the path, then updates the client
                        imagePath = Path.Combine(LocationSelector.SelectedValue, "Images");
                        savePath = Path.Combine(Request.PhysicalApplicationPath, imagePath);
                        fileName = Path.Combine(savePath, FileUpload1.FileName);
                        FileUpload1.SaveAs(fileName);
                        Userbox.Text = Page.User.Identity.Name;
                        Datebox.Text = DateTime.Now.ToShortDateString() + " at " + DateTime.Now.ToShortTimeString();
                        SqlDataSource1.Insert();
                        StatusLabel.Text = "Upload success!";
                    }
                    else
                    {   //Inform the client that a smaller file must be selected
                        StatusLabel.Text = "Upload Status: The photo is too large, it must be smaller than 1MB in size";
                    }
                }
                else
                {
                    StatusLabel.Text = "Upload Status: The file must be of type .JPG or an image file.";
                }
            }
            catch (Exception ex)
            {
                StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
            }
        }
    }
}