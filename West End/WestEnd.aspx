﻿<%@ Page Title="West End" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="WestEnd.aspx.cs" Inherits="West_End_WestEnd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .leftColumn
        {
            height: 2391px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <p>
        View, Upload, or comment on the latest photos for this cafe.</p>
    <p>
                <asp:Button ID="UploadGoTo" runat="server" Text="Upload" OnClick="UploadButton_Click" />
                <asp:SqlDataSource ID="picSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    
                    SelectCommand="SELECT [pid], [postedby], [postedwhen], [postedwhere], [picurl] FROM [gallery]" 
                    ConflictDetection="CompareAllValues" 
                    DeleteCommand="DELETE FROM [gallery] WHERE [pid] = @original_pid AND (([postedby] = @original_postedby) OR ([postedby] IS NULL AND @original_postedby IS NULL)) AND (([postedwhen] = @original_postedwhen) OR ([postedwhen] IS NULL AND @original_postedwhen IS NULL)) AND (([postedwhere] = @original_postedwhere) OR ([postedwhere] IS NULL AND @original_postedwhere IS NULL)) AND (([picurl] = @original_picurl) OR ([picurl] IS NULL AND @original_picurl IS NULL))" 
                    InsertCommand="INSERT INTO [gallery] ([postedby], [postedwhen], [postedwhere], [picurl]) VALUES (@postedby, @postedwhen, @postedwhere, @picurl)" 
                    OldValuesParameterFormatString="original_{0}" 
                    UpdateCommand="UPDATE [gallery] SET [postedby] = @postedby, [postedwhen] = @postedwhen, [postedwhere] = @postedwhere, [picurl] = @picurl WHERE [pid] = @original_pid AND (([postedby] = @original_postedby) OR ([postedby] IS NULL AND @original_postedby IS NULL)) AND (([postedwhen] = @original_postedwhen) OR ([postedwhen] IS NULL AND @original_postedwhen IS NULL)) AND (([postedwhere] = @original_postedwhere) OR ([postedwhere] IS NULL AND @original_postedwhere IS NULL)) AND (([picurl] = @original_picurl) OR ([picurl] IS NULL AND @original_picurl IS NULL))">
                    <DeleteParameters>
                        <asp:Parameter Name="original_pid" Type="Int32" />
                        <asp:Parameter Name="original_postedby" Type="String" />
                        <asp:Parameter Name="original_postedwhen" Type="String" />
                        <asp:Parameter Name="original_postedwhere" Type="String" />
                        <asp:Parameter Name="original_picurl" Type="String" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="postedby" Type="String" />
                        <asp:Parameter Name="postedwhen" Type="String" />
                        <asp:Parameter Name="postedwhere" Type="String" />
                        <asp:Parameter Name="picurl" Type="String" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="postedby" Type="String" />
                        <asp:Parameter Name="postedwhen" Type="String" />
                        <asp:Parameter Name="postedwhere" Type="String" />
                        <asp:Parameter Name="picurl" Type="String" />
                        <asp:Parameter Name="original_pid" Type="Int32" />
                        <asp:Parameter Name="original_postedby" Type="String" />
                        <asp:Parameter Name="original_postedwhen" Type="String" />
                        <asp:Parameter Name="original_postedwhere" Type="String" />
                        <asp:Parameter Name="original_picurl" Type="String" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                
                <asp:DataList ID="DataList1" runat="server" DataKeyField="pid" 
                DataSourceID="picSource" BackColor="#DEBA84" BorderColor="#DEBA84" 
                BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 
                GridLines="Both">
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <ItemStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <ItemTemplate>
                        &nbsp;<asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("picurl", "~/West End/Images/{0}") %>'
                            Height="200px" Width="315px" Tooltip="photo1"/>
                        <br />
                        Posted by
                        <asp:Label ID="postedbyLabel" runat="server" Text='<%# Eval("postedby") %>' />
                        on
                        <asp:Label ID="postedwhenLabel" runat="server" Text='<%# Eval("postedwhen") %>' />
                        .
                    </ItemTemplate>
                    <SelectedItemStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                </asp:DataList>
            </p>
    <asp:Panel ID="Panel1" runat="server">
        <asp:TextBox ID="LocationBox" runat="server" Visible="False"></asp:TextBox>
        <asp:TextBox ID="Datebox" runat="server" Visible="False"></asp:TextBox>
        <asp:TextBox ID="Userbox" runat="server" Visible="False"></asp:TextBox>
        <asp:SqlDataSource ID="CommentSource" runat="server" 
            ConflictDetection="CompareAllValues" 
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
            DeleteCommand="DELETE FROM [Comments] WHERE [commentID] = @original_commentID AND (([picURL] = @original_picURL) OR ([picURL] IS NULL AND @original_picURL IS NULL)) AND (([PostedWhere] = @original_PostedWhere) OR ([PostedWhere] IS NULL AND @original_PostedWhere IS NULL)) AND (([PostedBy] = @original_PostedBy) OR ([PostedBy] IS NULL AND @original_PostedBy IS NULL)) AND (([PostedWhen] = @original_PostedWhen) OR ([PostedWhen] IS NULL AND @original_PostedWhen IS NULL)) AND (([CommentsContents] = @original_CommentsContents) OR ([CommentsContents] IS NULL AND @original_CommentsContents IS NULL))" 
            InsertCommand="INSERT INTO [Comments] ([picURL], [PostedWhere], [PostedBy], [PostedWhen], [CommentsContents]) VALUES (@picURL, @PostedWhere, @PostedBy, @PostedWhen, @CommentsContents)" 
            OldValuesParameterFormatString="original_{0}" 
            SelectCommand="SELECT [commentID], [picURL], [PostedWhere], [PostedBy], [PostedWhen], [CommentsContents] FROM [Comments]" 
            UpdateCommand="UPDATE [Comments] SET [picURL] = @picURL, [PostedWhere] = @PostedWhere, [PostedBy] = @PostedBy, [PostedWhen] = @PostedWhen, [CommentsContents] = @CommentsContents WHERE [commentID] = @original_commentID AND (([picURL] = @original_picURL) OR ([picURL] IS NULL AND @original_picURL IS NULL)) AND (([PostedWhere] = @original_PostedWhere) OR ([PostedWhere] IS NULL AND @original_PostedWhere IS NULL)) AND (([PostedBy] = @original_PostedBy) OR ([PostedBy] IS NULL AND @original_PostedBy IS NULL)) AND (([PostedWhen] = @original_PostedWhen) OR ([PostedWhen] IS NULL AND @original_PostedWhen IS NULL)) AND (([CommentsContents] = @original_CommentsContents) OR ([CommentsContents] IS NULL AND @original_CommentsContents IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_commentID" Type="Int32" />
                <asp:Parameter Name="original_picURL" Type="String" />
                <asp:Parameter Name="original_PostedWhere" Type="String" />
                <asp:Parameter Name="original_PostedBy" Type="String" />
                <asp:Parameter Name="original_PostedWhen" Type="String" />
                <asp:Parameter Name="original_CommentsContents" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="Image1" Name="picURL" PropertyName="Name" 
                    Type="String" />
                <asp:ControlParameter ControlID="Locationbox" Name="PostedWhere" 
                    PropertyName="text" Type="String" />
                <asp:ControlParameter ControlID="Userbox" Name="PostedBy" PropertyName="text" 
                    Type="String" />
                <asp:ControlParameter ControlID="Datebox" Name="PostedWhen" PropertyName="text" 
                    Type="String" />
                <asp:ControlParameter ControlID="Commentbox" Name="CommentsContents" 
                    Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="picURL" Type="String" />
                <asp:Parameter Name="PostedWhere" Type="String" />
                <asp:Parameter Name="PostedBy" Type="String" />
                <asp:Parameter Name="PostedWhen" Type="String" />
                <asp:Parameter Name="CommentsContents" Type="String" />
                <asp:Parameter Name="original_commentID" Type="Int32" />
                <asp:Parameter Name="original_picURL" Type="String" />
                <asp:Parameter Name="original_PostedWhere" Type="String" />
                <asp:Parameter Name="original_PostedBy" Type="String" />
                <asp:Parameter Name="original_PostedWhen" Type="String" />
                <asp:Parameter Name="original_CommentsContents" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:DataList ID="DataList2" runat="server" BackColor="#DEBA84" 
            BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
            CellSpacing="2" DataKeyField="commentID" DataSourceID="CommentSource" 
            GridLines="Both" Height="195px">
            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
            <ItemStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
            <ItemTemplate>
                <br />
                <br />
                <br />
                Posted by:
                <asp:Label ID="PostedByLabel" runat="server" Text='<%# Eval("PostedBy") %>' />
                , on:
                <asp:Label ID="PostedWhenLabel" runat="server" 
                    Text='<%# Eval("PostedWhen") %>' />
                <br />
                &nbsp;<asp:Label ID="CommentsContentsLabel" runat="server" 
                    Text='<%# Eval("CommentsContents") %>' />
                <br />
                <br />
            </ItemTemplate>
            <SelectedItemStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
        </asp:DataList>
        &nbsp;</asp:Panel>
    <p>
        &nbsp;</p>
        
</asp:Content>
