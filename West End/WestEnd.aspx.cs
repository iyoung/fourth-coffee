﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class West_End_WestEnd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void UploadButton_Click(object sender, EventArgs e)
    {
        Server.Transfer("~/Upload.aspx");
    }
    protected void PostComment_Click(object sender, EventArgs e)
    {
        Userbox.Text = Page.User.Identity.Name;
        Datebox.Text = DateTime.Now.ToShortDateString() + " at " + DateTime.Now.ToShortTimeString();
        LocationBox.Text= Page.Title;
        CommentSource.Insert();
    }
    protected void ReadComments_Click(object sender, EventArgs e)
    {
 
    }

}