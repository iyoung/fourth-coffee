﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Upload.aspx.cs" Inherits="Upload" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:Label ID="LocationLabel" runat="server"></asp:Label>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
        DeleteCommand="DELETE FROM [gallery] WHERE [pid] = @original_pid AND (([postedby] = @original_postedby) OR ([postedby] IS NULL AND @original_postedby IS NULL)) AND (([postedwhen] = @original_postedwhen) OR ([postedwhen] IS NULL AND @original_postedwhen IS NULL)) AND (([postedwhere] = @original_postedwhere) OR ([postedwhere] IS NULL AND @original_postedwhere IS NULL)) AND (([picurl] = @original_picurl) OR ([picurl] IS NULL AND @original_picurl IS NULL))" 
        InsertCommand="INSERT INTO [gallery] ([postedby], [postedwhen], [postedwhere], [picurl]) VALUES (@postedby, @postedwhen, @postedwhere, @picurl)" 
        SelectCommand="SELECT [postedby], [postedwhen], [postedwhere], [picurl], [pid] FROM [gallery]" 
        
        UpdateCommand="UPDATE [gallery] SET [postedby] = @postedby, [postedwhen] = @postedwhen, [postedwhere] = @postedwhere, [picurl] = @picurl WHERE [pid] = @original_pid AND (([postedby] = @original_postedby) OR ([postedby] IS NULL AND @original_postedby IS NULL)) AND (([postedwhen] = @original_postedwhen) OR ([postedwhen] IS NULL AND @original_postedwhen IS NULL)) AND (([postedwhere] = @original_postedwhere) OR ([postedwhere] IS NULL AND @original_postedwhere IS NULL)) AND (([picurl] = @original_picurl) OR ([picurl] IS NULL AND @original_picurl IS NULL))" 
        ConflictDetection="CompareAllValues" 
        OldValuesParameterFormatString="original_{0}">
        <DeleteParameters>
            <asp:Parameter Name="original_pid" Type="Int32" />
            <asp:Parameter Name="original_postedby" Type="String" />
            <asp:Parameter Name="original_postedwhen" Type="String" />
            <asp:Parameter Name="original_postedwhere" Type="String" />
            <asp:Parameter Name="original_picurl" Type="String" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="postedby" Type="String" />
            <asp:Parameter Name="postedwhen" Type="String" />
            <asp:Parameter Name="postedwhere" Type="String" />
            <asp:Parameter Name="picurl" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="postedby" Type="String" />
            <asp:Parameter Name="postedwhen" Type="String" />
            <asp:Parameter Name="postedwhere" Type="String" />
            <asp:Parameter Name="picurl" Type="String" />
            <asp:Parameter Name="original_pid" Type="Int32" />
            <asp:Parameter Name="original_postedby" Type="String" />
            <asp:Parameter Name="original_postedwhen" Type="String" />
            <asp:Parameter Name="original_postedwhere" Type="String" />
            <asp:Parameter Name="original_picurl" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    
    <asp:TextBox ID="Userbox" runat="server" Visible="False"></asp:TextBox>
    
    <asp:TextBox ID="Datebox" runat="server" Visible="False"></asp:TextBox>
    
<br />
    
<asp:CheckBoxList ID="LocationSelector" runat="server">
    <asp:ListItem>West End</asp:ListItem>
    <asp:ListItem>Central</asp:ListItem>
    <asp:ListItem>East End</asp:ListItem>
    <asp:ListItem>South Side</asp:ListItem>
</asp:CheckBoxList>
    
<br />
<asp:RequiredFieldValidator ID="FileUploadValidator" runat="server" 
        ControlToValidate="FileUpload1" ErrorMessage="A Selection must be made to upload"></asp:RequiredFieldValidator>
    <br />
    <asp:FileUpload ID="FileUpload1" runat="server" />
    <br />
    <br />
    <asp:Button ID="SendButton" runat="server" Text="Send Photo" onclick="SendButton_Click" />
    <br />
    <br />
    <asp:Label runat="server" ID="StatusLabel" Text="Upload Status: " />
</asp:Content>

